package org.hackmelbourne.spaceshipsimulator.controlconsole.app;

import android.app.Service;
import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.ProgressDialog;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.nsd.NsdServiceInfo;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.ResolveListener;
import android.net.nsd.NsdManager.DiscoveryListener;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import java.net.NetworkInterface;


public class ZeroconfHelper {

    private DiscoveryListener mNsdDiscoveryListener;
    private ResolveListener mNsdResolveListener;
    private IZeroconfHelperListeners mZeroconfHelperListeners;
    private String mZeroconfServiceType;

    public ZeroconfHelper(String serviceType, IZeroconfHelperListeners listeners) {

        // Save the service type that we need to be searching for
        mZeroconfServiceType = serviceType;

        // Store a link to the caller object that must implement our event listeners
        mZeroconfHelperListeners = listeners;

        // Initialise a Discovery object to locate our simulator server on the Wifi network
        // using Multicast DNS broadcasts (mDNS / Avahi / Bonjour).
        mNsdDiscoveryListener = new DiscoveryListener() {
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {

            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {

            }

            @Override
            public void onDiscoveryStarted(String serviceType) {

            }

            @Override
            public void onDiscoveryStopped(String serviceType) {

            }

            @Override
            public void onServiceFound(NsdServiceInfo serviceInfo) {

            }

            @Override
            public void onServiceLost(NsdServiceInfo serviceInfo) {

            }
        };

        // Initialise the Resolver object - once we've identified the server by it's
        // service name, we need to obtain detailed information (such as it's IP and service
        // parameters [port numbers, etc])...
        mNsdResolveListener = new ResolveListener() {
            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {

            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {

            }
        };
    }

    public interface IZeroconfHelperListeners {
        public void onZeroconfServiceFound(String serviceType, NsdServiceInfo serviceInfo);
        public void onZeroconfServiceLost(String serviceType, NsdServiceInfo serviceInfo);
        public void onZeroconfException(String serviceType, Exception exception);
    }
}
