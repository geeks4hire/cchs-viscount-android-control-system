package org.hackmelbourne.spaceshipsimulator.controlconsole.app;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.ProgressDialog;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.nsd.NsdServiceInfo;
import android.net.nsd.NsdManager;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import java.net.NetworkInterface;


public class NetworkHelper {
    public static String TAG = "NetworkHelpers";
    private static INetworkHelperListeners networkListeners;
    private Context mContext;
    private boolean isWifiConnected = false;
    private String wifiIpAddress = null;
    private String wifiDnsServer1 = null;
    private String wifiDnsServer2 = null;
    private String wifiGatewayAddress = null;
    private String wifiNetmask = null;
    private String wifiSSID = null;

    public NetworkHelper(Context context) {
        this.mContext = context;
    }

    public void UpdateConnectionStatus() {

        // Firstly query the connection manager to ascertain if WiFi is enabled and connected

        ConnectivityManager connMgr = (ConnectivityManager)
                this.mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo != null) {
            isWifiConnected = networkInfo.isConnected();
        }

        // If we are connected via Wi-Fi, time to probe the WifiManager to find out more info
        // about our connection (IP/DNS/Router/Netmask/Wireless Lan SSID)...

        if (isWifiConnected) {

            WifiManager wifiMgr = (WifiManager)
                    this.mContext.getSystemService(Context.WIFI_SERVICE);
            if (wifiMgr != null) {

                // Query to find out the currently connected WiFi SSID (network name)

                if (wifiMgr.getConnectionInfo().getSSID() != null) {
                    wifiSSID = wifiMgr.getConnectionInfo().getSSID();
                }

                // Look up the results of the last DHCP allocated client IP details (if there
                // are any that is) - otherwise null out the variable values.

                DhcpInfo wifiDhcpInfo = wifiMgr.getDhcpInfo();
                if (wifiDhcpInfo != null) {
                    wifiIpAddress = (wifiDhcpInfo.ipAddress > 0) ? intToIp(wifiDhcpInfo.ipAddress) : null;
                    wifiDnsServer1 = (wifiDhcpInfo.dns1 > 0) ? intToIp(wifiDhcpInfo.dns1) : null;
                    wifiDnsServer2 = (wifiDhcpInfo.dns2 > 0) ? intToIp(wifiDhcpInfo.dns2) : null;
                    wifiGatewayAddress = (wifiDhcpInfo.gateway > 0) ? intToIp(wifiDhcpInfo.gateway) : null;
                    wifiNetmask = (wifiDhcpInfo.netmask > 0) ? intToIp(wifiDhcpInfo.netmask) : null;
                }
            }
        }

        Log.d(TAG, "Wifi Connection State:  " + isWifiConnected);
        Log.d(TAG, "Wifi Network SSID:      " + wifiSSID);
        Log.d(TAG, "Wifi Client IP:         " + wifiIpAddress);
        Log.d(TAG, "Wifi Client Netmask:    " + wifiNetmask);
        Log.d(TAG, "Wifi Gateway Address:   " + wifiGatewayAddress);
        Log.d(TAG, "Wifi Primary DNS:       " + wifiDnsServer1);
        Log.d(TAG, "Wifi Secondary DNS:     " + wifiDnsServer2);

    }

    public boolean IsWifiConnected() {
        return (this.IsWifiConnected(false));
    }

    public Boolean IsWifiConnected(Boolean forceRefresh) {
        if (forceRefresh) {
            this.UpdateConnectionStatus();
        }
        return (isWifiConnected);
    }

    private String intToIp(int addr) {
        return ((addr & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF));
    }

    public interface INetworkHelperListeners {
        public NsdServiceInfo onNetworkServerDiscovery();

        public void onNetworkServiceAvailable();

        public void onNetworkServiceUnavailable();

        public NetworkInfo onNetworkConnectionAvailable();

        public NetworkInfo onNetworkConnectionUnavailable();

        public DhcpInfo onLocalDhcpDiscovery();
    }
}
