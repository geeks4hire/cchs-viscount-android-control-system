package org.hackmelbourne.spaceshipsimulator.controlconsole.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;

import eu.inmite.android.lib.dialogs.BaseDialogFragment;
import eu.inmite.android.lib.dialogs.SimpleDialogFragment;

public class ConnectionSettingsDialog extends SimpleDialogFragment {
    public static String TAG = "Connection Settings Dialog";
    private IConnectionSettingsDialogListeners dialogListeners;
    private View mDialogView;

    public ConnectionSettingsDialog() {
        // Required empty public constructor
    }

    public ConnectionSettingsDialog(IConnectionSettingsDialogListeners dialogListeners) {
        super();
        this.dialogListeners = dialogListeners;
    }

    public static ConnectionSettingsDialog newInstance(IConnectionSettingsDialogListeners dialogListeners) {
        return (new ConnectionSettingsDialog(dialogListeners));
    }

    public static void show(FragmentActivity activity) {
        new ConnectionSettingsDialog().show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public BaseDialogFragment.Builder build(BaseDialogFragment.Builder builder) {
        builder.setTitle(R.string.title_dialog_connection_settings);

        // Inflate the layout for this fragment
        mDialogView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_connection_settings_dialog, null);

        // Bind Auto/Manual server settings switch to show/hide relevant sections of settings dialog
        ((Switch) mDialogView.findViewById(R.id.connection_settings_server_auto_or_manual_switch))
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        View autoSettingsSection = buttonView.getRootView().findViewById(R.id.connection_settings_auto_server_settings_layout);
                        View manualSettingsSection = buttonView.getRootView().findViewById(R.id.connection_settings_manual_server_settings_layout);

                        if (isChecked) {
                            //Switch is set to automatically obtain server settings via Zeroconf
                            manualSettingsSection.setVisibility(View.GONE);
                            autoSettingsSection.setVisibility(View.VISIBLE);

                        } else {
                            //Switch is set to manually obtain server settings from user...
                            autoSettingsSection.setVisibility(View.GONE);
                            manualSettingsSection.setVisibility(View.VISIBLE);
                        }
                    }
                });

        // Bind Auto-Detect server button to listener interface
        ((Button) mDialogView.findViewById(R.id.connection_settings_auto_detect_server_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (dialogListeners != null) {
                            dialogListeners.connectionSettingsAutoDetectServerClicked();
                        }
                    }
                });

        // Bind Verify Server button to listener interface
        ((Button) mDialogView.findViewById(R.id.connection_settings_verify_connection_button))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (dialogListeners != null) {
                            dialogListeners.connectionSettingsVerifyManualServerClicked();
                        }
                    }
                });

        // Bind IP Address keypress validation filter to Manual Server Host IP address EditText field
        EditText edAddress = (EditText) mDialogView.findViewById(R.id.connection_settings_manual_server_host_text);
        edAddress.setKeyListener(IPAddressKeyListener.getInstance());

        // Preset form field values to saved preferences (or to defaults if no preferences saved yet)...
        loadFormValuesFromSharedPreferences();

        builder.setView(mDialogView);

        // Bind Save button to listener interface
        builder.setPositiveButton(R.string.label_save, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFormValuesToSharedPreferences();
                if (dialogListeners != null) {
                    dialogListeners.connectionSettingsDialogOkClicked();
                }
                dismiss();
            }
        });

        // Bind Cancel button to listener interface
        builder.setNegativeButton(R.string.label_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogListeners != null) {
                    dialogListeners.connectionSettingsDialogCancelClicked();
                }
                dismiss();
            }
        });

        return builder;
    }

    private void loadFormValuesFromSharedPreferences() {
        SharedPreferences sharedPrefs;
        SharedPreferences.Editor editor;
        if (mDialogView != null) {
            try {
                sharedPrefs = getActivity().getBaseContext().getSharedPreferences(getString(R.string.preferences_connection_settings_file), Context.MODE_PRIVATE);

                EditText serverRefreshMins = (EditText) mDialogView.findViewById(R.id.connection_settings_refresh_interval_text);
                Switch serverEnableMediaStreaming = (Switch) mDialogView.findViewById(R.id.connection_settings_enable_audio_video_link_switch);
                Switch serverEnableAutoConfigure = (Switch) mDialogView.findViewById(R.id.connection_settings_server_auto_or_manual_switch);
                EditText serverAutoDetectedHostIpAddress = (EditText) mDialogView.findViewById(R.id.connection_settings_manual_server_host_text);
                EditText serverManualEntryHostIpAddress = (EditText) mDialogView.findViewById(R.id.connection_settings_manual_server_host_text);
                EditText serverHttpPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_web_server_port_text);
                EditText serverMqttPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_mqtt_server_port_text);
                EditText serverSipPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_sip_server_port_text);
                RelativeLayout serverAutoSettings = (RelativeLayout) mDialogView.findViewById(R.id.connection_settings_auto_server_settings_layout);
                RelativeLayout serverManualSettings = (RelativeLayout) mDialogView.findViewById(R.id.connection_settings_manual_server_settings_layout);

                String serverModeAutoManual = sharedPrefs.getString(getString(R.string.preferences_server_mode_auto_manual), getString(R.string.default_server_settings_mode_auto_manual)).toLowerCase();

                serverRefreshMins.setText((CharSequence) String.valueOf(sharedPrefs.getInt(getString(R.string.preferences_server_refresh_minutes), Integer.parseInt(getString(R.string.default_server_refresh_minutes)))));
                serverEnableMediaStreaming.setChecked(sharedPrefs.getBoolean(getString(R.string.preferences_enable_media_streaming), Boolean.getBoolean(getString(R.string.default_enable_media_streaming))));
                serverEnableAutoConfigure.setChecked((Boolean) (serverModeAutoManual == "auto"));
                if (serverModeAutoManual == "auto") {
                    serverAutoSettings.setVisibility(View.VISIBLE);
                    serverManualSettings.setVisibility(View.GONE);
                    serverAutoDetectedHostIpAddress.setText((CharSequence) sharedPrefs.getString(getString(R.string.preferences_server_host_address), new String("")));
                    serverManualEntryHostIpAddress.setText("");
                } else {
                    serverAutoSettings.setVisibility(View.GONE);
                    serverManualSettings.setVisibility(View.VISIBLE);
                    serverAutoDetectedHostIpAddress.setText("");
                    serverManualEntryHostIpAddress.setText((CharSequence) sharedPrefs.getString(getString(R.string.preferences_server_host_address), new String("")));
                }
                serverHttpPortNumber.setText((CharSequence) String.valueOf(sharedPrefs.getInt(getString(R.string.preferences_server_http_port), Integer.parseInt(getString(R.string.default_server_http_port)))));
                serverMqttPortNumber.setText((CharSequence) String.valueOf(sharedPrefs.getInt(getString(R.string.preferences_server_mqtt_port), Integer.parseInt(getString(R.string.default_server_mqtt_port)))));
                serverSipPortNumber.setText((CharSequence) String.valueOf(sharedPrefs.getInt(getString(R.string.preferences_server_sip_port), Integer.parseInt(getString(R.string.default_server_sip_port)))));
            } catch (Exception ex) {
                Log.e(this.TAG, "Error loading shared preferences into Connection Settings dialog", ex);
            } finally {
                sharedPrefs = null;
            }
        }
    }

    private void saveFormValuesToSharedPreferences() {
        SharedPreferences sharedPrefs;
        SharedPreferences.Editor editor;
        if (mDialogView != null) {
                try {
                    sharedPrefs = getActivity().getBaseContext().getSharedPreferences(getString(R.string.preferences_connection_settings_file), Context.MODE_PRIVATE);
                    editor = sharedPrefs.edit();

                    EditText serverRefreshMins = (EditText) mDialogView.findViewById(R.id.connection_settings_refresh_interval_text);
                    Switch serverEnableMediaStreaming = (Switch) mDialogView.findViewById(R.id.connection_settings_enable_audio_video_link_switch);
                    Switch serverEnableAutoConfigure = (Switch) mDialogView.findViewById(R.id.connection_settings_server_auto_or_manual_switch);
                    EditText serverAutoDetectedHostIpAddress = (EditText) mDialogView.findViewById(R.id.connection_settings_manual_server_host_text);
                    EditText serverManualEntryHostIpAddress = (EditText) mDialogView.findViewById(R.id.connection_settings_manual_server_host_text);
                    EditText serverHttpPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_web_server_port_text);
                    EditText serverMqttPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_mqtt_server_port_text);
                    EditText serverSipPortNumber = (EditText) mDialogView.findViewById(R.id.connection_settings_sip_server_port_text);

                    editor.putInt(getString(R.string.preferences_server_refresh_minutes), Integer.parseInt(serverRefreshMins.getText().toString()));
                    editor.putBoolean(getString(R.string.preferences_enable_media_streaming), serverEnableMediaStreaming.isChecked());
                    editor.putString(getString(R.string.preferences_server_mode_auto_manual), (serverEnableAutoConfigure.isChecked() ? "auto" : "manual"));
                    if (serverEnableAutoConfigure.isChecked()) {
                        // The user has chosen to auto-detect the server side IP and ports using ZeroConf

                        if (serverAutoDetectedHostIpAddress.getText().toString().length() == 0) {
                            // TODO: Abort saving and alert user to missing IP address (they either need to click the "Auto-Detect Server" button, or switch to manual settings and fill them in
                            // The user has set to auto-detect the server IP, but it seems we haven't yet detected one!
                            Log.e(this.TAG, "Saving form values to Shared Preferences file - Auto Detect server settings was switched on, but we have no server host IP to save!");
                            editor.putString(getString(R.string.preferences_server_host_address), "");
                        } else {
                            editor.putString(getString(R.string.preferences_server_host_address), serverAutoDetectedHostIpAddress.getText().toString());
                        }
                    } else {
                        // The user has chosen to manually enter the server side IP and ports via settings dialog

                        if (serverManualEntryHostIpAddress.getText().toString().length() == 0) {
                            // TODO: Proper IP Address validation before saving
                            // The user has set to auto-detect the server IP, but it seems we haven't yet detected one!
                            Log.e(this.TAG, "Saving form values to Shared Preferences file - Auto Detect server settings was switched off, but user has not entered a manual server host IP to save!");
                            editor.putString(getString(R.string.preferences_server_host_address), "");
                        } else {
                            editor.putString(getString(R.string.preferences_server_host_address), serverManualEntryHostIpAddress.getText().toString());
                        }

                        if ((serverHttpPortNumber.getText().toString().length() == 0) || (Integer.parseInt(serverHttpPortNumber.getText().toString()) < 80) || (Integer.parseInt(serverHttpPortNumber.getText().toString()) > 65535)) {
                            // Either an empty port number (or an invalid port number) has been entered - save the default port value for this service instead
                            Log.i(this.TAG, "Saving form values to Shared Preferences file - HTTP Server Port number was either blank or invalid - saving default port number instead.");
                            editor.putInt(getString(R.string.preferences_server_http_port), Integer.parseInt(getString(R.string.default_server_http_port)));
                        } else {
                            editor.putInt(getString(R.string.preferences_server_http_port), Integer.parseInt(serverHttpPortNumber.getText().toString()));
                        }

                        if ((serverMqttPortNumber.getText().toString().length() == 0) || (Integer.parseInt(serverMqttPortNumber.getText().toString()) < 1024) || (Integer.parseInt(serverMqttPortNumber.getText().toString()) > 65535)) {
                            // Either an empty port number (or an invalid port number) has been entered - save the default port value for this service instead
                            Log.i(this.TAG, "Saving form values to Shared Preferences file - MQTT Server Port number was either blank or invalid - saving default port number instead.");
                            editor.putInt(getString(R.string.preferences_server_mqtt_port), Integer.parseInt(getString(R.string.default_server_mqtt_port)));
                        } else {
                            editor.putInt(getString(R.string.preferences_server_mqtt_port), Integer.parseInt(serverMqttPortNumber.getText().toString()));
                        }

                        if ((serverSipPortNumber.getText().toString().length() == 0) || (Integer.parseInt(serverSipPortNumber.getText().toString()) < 1024) || (Integer.parseInt(serverSipPortNumber.getText().toString()) > 65535)) {
                            // Either an empty port number (or an invalid port number) has been entered - save the default port value for this service instead
                            Log.i(this.TAG, "Saving form values to Shared Preferences file - SIP Server Port number was either blank or invalid - saving default port number instead.");
                            editor.putInt(getString(R.string.preferences_server_sip_port), Integer.parseInt(getString(R.string.default_server_sip_port)));
                        } else {
                            editor.putInt(getString(R.string.preferences_server_sip_port), Integer.parseInt(serverSipPortNumber.getText().toString()));
                        }
                    }

                    // Save the new preferences
                    editor.commit();

                } catch (Exception ex) {
                    Log.e(this.TAG, "Error saving shared preferences from Connection Settings dialog", ex);
                } finally {
                    editor = null;
                    sharedPrefs = null;
                }
            }

    }

    public interface IConnectionSettingsDialogListeners {
        public void connectionSettingsDialogOkClicked();

        public void connectionSettingsDialogCancelClicked();

        public void connectionSettingsAutoDetectServerClicked();

        public void connectionSettingsVerifyManualServerClicked();
    }
}
