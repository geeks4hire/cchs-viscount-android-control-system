package org.hackmelbourne.spaceshipsimulator.controlconsole.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, ConnectionSettingsDialog.IConnectionSettingsDialogListeners {
    private ViewPager mViewPager;
    private List<Fragment> pageFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide the app title text from displaying in the action bar (wastes too much space!)
        getActionBar().setDisplayShowTitleEnabled(false);

        // Set the view for our app

        setContentView(R.layout.activity_main);

        // Lock the orientation in Landscape (this control console app
        // wouldn't be all that intuitive to use in Portrait mode, so
        // I haven't bothered with flexible fragment layouts for tall
        // display orientation).

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Prevent screensaver/lock??

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set up the action bar.

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Pre-load the pager fragments and store references in an array to
        // optimise transition responsiveness

        pageFragments = loadPageFragments();

        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(new MyPageAdapter(getSupportFragmentManager(), pageFragments));
        mViewPager.setAnimation(null);

        // Set the initial pager view to the first item

        mViewPager.setCurrentItem(0, false);

        // When swiping between different sections, select the corresponding
        // tab.

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                // update the action bar selected tab to correspond to the new
                // page when the user drags/scrolls

                actionBar.setSelectedNavigationItem(position);
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });

        createActionBarTabs(actionBar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds menu items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showConnectionSettingsDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.

        mViewPager.setCurrentItem(tab.getPosition(), true);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    private List<Fragment> loadPageFragments() {
        // Initialise and inflate the layouts for the different control console pages
        // (stored as Fragments). Persist them into an array so that they can be tracked
        // and won't have repeated attempts to initialise them (lazy approach calls newInstance
        // every time a page is requested, whereas this method caches pointers to each inflated
        // layout fragment so it's only inflated/initialised once)...

        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(SimulatorScenariosFragment.newInstance());
        fList.add(SimulatorProgressFragment.newInstance());
        fList.add(SimulatorCockpitFragment.newInstance());
        fList.add(SimulatorCommunicationsFragment.newInstance());
        fList.add(SimulatorSettingsFragment.newInstance());

        return fList;
    }

    private void createActionBarTabs(ActionBar actionBar) {
        // Create the navigation tabs for the action bar to provide quick access
        // to all of the control console sections (implemented below as Fragments)

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.action_simulator_scenarios)
                        .setIcon(R.drawable.ic_action_simulator_scenarios)
                        .setTabListener(this)
        );

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.action_simulator_progress)
                        .setIcon(R.drawable.ic_action_simulator_progress)
                        .setTabListener(this)
        );

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.action_simulator_cockpit)
                        .setIcon(R.drawable.ic_action_simulator_cockpit)
                        .setTabListener(this)
        );

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.action_simulator_comms)
                        .setIcon(R.drawable.ic_action_simulator_comms)
                        .setTabListener(this)
        );

        actionBar.addTab(
                actionBar.newTab()
                        .setText(R.string.action_simulator_settings)
                        .setIcon(R.drawable.ic_action_simulator_settings)
                        .setTabListener(this)
        );

    }

    //
    // Display Connection Settings dialog
    //
    public void showConnectionSettingsDialog() {
            ConnectionSettingsDialog connectionSettingsDialog = new ConnectionSettingsDialog(this);
            connectionSettingsDialog.setCancelable(false);
            connectionSettingsDialog.show(getSupportFragmentManager(), ConnectionSettingsDialog.TAG);
    }

    // Handle results from Connection Settings Dialog (Save Button Clicked)
    public void connectionSettingsDialogOkClicked() {
        Toast.makeText(getBaseContext(), "Connection Settings dialog - OK button clicked", Toast.LENGTH_SHORT).show();
    }

    // Handle results from Connection Settings Dialog (Cancel Button Clicked)
    public void connectionSettingsDialogCancelClicked() {
        Toast.makeText(getBaseContext(), "Connection Settings dialog - CANCEL button clicked", Toast.LENGTH_SHORT).show();
    }

    // Handle results from Connection Settings Dialog (Auto-Detect Server Button Clicked)
    public void connectionSettingsAutoDetectServerClicked() {
        //Toast.makeText(getBaseContext(), "Connection Settings dialog - VERIFY SERVER button clicked", Toast.LENGTH_SHORT).show();
            final ProgressDialog settingsProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Attempting server auto-detection ...", true);
            settingsProgressDialog.setCancelable(true);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Here you should write your time consuming task...
                        // Let the progress ring for 10 seconds...
                        Thread.sleep(10000);
                    } catch (Exception e) {

                    }
                    settingsProgressDialog.dismiss();
                }
            }).start();
    }

    // Handle results from Connection Settings Dialog (Verify Manual Server Settings Button Clicked)
    public void connectionSettingsVerifyManualServerClicked() {
        //Toast.makeText(getBaseContext(), "Connection Settings dialog - VERIFY SERVER button clicked", Toast.LENGTH_SHORT).show();
        final ProgressDialog settingsProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Verifying connection to server ...", true);
        settingsProgressDialog.setCancelable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // Here you should write your time consuming task...
                    // Let the progress ring for 10 seconds...
                    Thread.sleep(6000);
                } catch (Exception e) {

                }
                settingsProgressDialog.dismiss();
            }
        }).start();
    }

    //
    // Page Segment for choosing training/mission scenarios
    //
    public static class SimulatorScenariosFragment extends Fragment {
        public SimulatorScenariosFragment() {
        }

        public static SimulatorScenariosFragment newInstance() {
            return (new SimulatorScenariosFragment());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return ((View) inflater.inflate(R.layout.fragment_simulator_scenarios, container, false));
        }
    }

    //
    // Page Segment for monitoring mission progress and spaceship systems status
    //
    public static class SimulatorProgressFragment extends Fragment {
        public SimulatorProgressFragment() {
        }

        public static SimulatorProgressFragment newInstance() {
            return (new SimulatorProgressFragment());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return ((View) inflater.inflate(R.layout.fragment_simulator_progress, container, false));
        }
    }

    //
    // Page Segment for viewing instrumentation states in the cockpit
    //
    public static class SimulatorCockpitFragment extends Fragment {
        public SimulatorCockpitFragment() {
        }

        public static SimulatorCockpitFragment newInstance() {
            return (new SimulatorCockpitFragment());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return ((View) inflater.inflate(R.layout.fragment_simulator_cockpit, container, false));
        }
    }

    //
    // Page Segment for communicating with the crew via audio (radio) or video
    // and also viewing the cockpit camera (A/V communications handled by VoIP)
    //
    public static class SimulatorCommunicationsFragment extends Fragment {
        public SimulatorCommunicationsFragment() {
        }

        public static SimulatorCommunicationsFragment newInstance() {
            return (new SimulatorCommunicationsFragment());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return ((View) inflater.inflate(R.layout.fragment_simulator_comms, container, false));
        }
    }

    //
    // Page Segment for modifying simulator settings
    //
    public static class SimulatorSettingsFragment extends Fragment {
        public SimulatorSettingsFragment() {
        }

        public static SimulatorSettingsFragment newInstance() {
            return (new SimulatorSettingsFragment());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return ((View) inflater.inflate(R.layout.fragment_simulator_settings, container, false));
        }
    }

    //
    // Custom FragmentPageAdapter used for view paging that initialises each
    // page view by inflating/rendering each fragment, and storing their
    // references in an List (much lower overhead than calling "newInstance"
    // each and every time one wants access to a page fragment using the regular
    // FragmentPagerAdapter component)
    //
    public class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }
}